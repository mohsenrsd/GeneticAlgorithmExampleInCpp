# GeneticAlgorithmExampleInCpp

This code is an simple implementation of Genetic Algorithms.
the program takes two variables from the input and then calculates the maximum of the sinc function.
You can compile this project using the following commands:
g++ -o out.exe max_sinc_fun.cpp genetic_algorithm.cpp