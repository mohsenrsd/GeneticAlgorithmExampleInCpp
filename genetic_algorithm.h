// Max Of Sinc Function - Using Genetic Algorithm
// codeblog.ir
// mohsenrsd@gmail.com

#include <stdlib.h>
#include <cmath>
#include <algorithm>
#include <vector>
using std::vector;

#include <iomanip>
using std::setprecision;

#include "population_row.h"

#define PI 3.1415926535897932385
#define infinity 9000000

typedef	struct Bit {
	unsigned ib1:1;
	unsigned ib2:1;
	unsigned ib3:1;
	unsigned ib4:1;
	unsigned ib5:1;
	unsigned db1:1;
	unsigned db2:1;
	unsigned db3:1;
	unsigned db4:1;
	unsigned db5:1;
	unsigned db6:1;
	unsigned db7:1;
	unsigned db8:1;
	unsigned db9:1;
	unsigned db10:1;
}Bits;

class GeneticAlgo {

public:
	GeneticAlgo(int itrNum, int popCount);
	double findMaxOfFunction();
	static	bool myDataSortPredicate(const PopulationRow& d1, const PopulationRow& d2);
	vector <PopulationRow> pupPop;
	double getMaximum();
private:
	double Max;
	int iterateNum;
	int populationCount;
	int rangedRand( int range_min, int range_max );
	vector <PopulationRow> setAllF(vector <PopulationRow> pop);
	vector <PopulationRow> initialize(vector <PopulationRow> pop);
	double sinc (double x, double y);
	vector <PopulationRow> selection1( vector <PopulationRow> pop);
	vector <PopulationRow> selection2( vector <PopulationRow> pop);
	vector <PopulationRow> crossOverpopulation( vector <PopulationRow> coPop );
	vector <PopulationRow> crossOver(vector <PopulationRow> crossPop);
	vector <PopulationRow> mutationpopulation( vector <PopulationRow> pop);
	double mutation(double f);
	Bits convertdoubleToBitStruct( double x );
	double convertBitStructTodouble (Bits b);
};

