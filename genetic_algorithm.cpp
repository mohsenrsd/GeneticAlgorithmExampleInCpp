// Max Of Sinc Function - Using Genetic Algorithm - By Mohsen Rashidi
// codeblog.ir
// mohsenrsd@gmail.com

#include "genetic_algorithm.h"

using std::max;

GeneticAlgo::GeneticAlgo(int itrNum, int popCount):
	iterateNum(itrNum), populationCount(popCount)
{}

double GeneticAlgo::findMaxOfFunction() {
	vector <PopulationRow> pop1,pop2,pop3,pop4;

	//initialize pop1
	pop1 = initialize(pop1);

	pupPop = pop1;
	std::sort(pop1.begin(), pop1.end(), myDataSortPredicate);
	Max = pop1[0].f;

	for( int i = 1 ; i <= iterateNum ; i++ )
	{
		pop2 =  selection1(pop1);

		// pop3 = crossover(pop2) + pop2
		vector <PopulationRow> cPop2 = crossOverpopulation(pop2);
		pop3 = pop2;
		pop3.insert(pop3.end(), cPop2.begin(), cPop2.end() );

		// pop4 = mutation(pop2) + pop3
		vector <PopulationRow> mPop2 = mutationpopulation(pop2);
		pop4 = pop3;
		pop4.insert(pop4.end(), mPop2.begin(), mPop2.end() );
		pop1 = selection2(pop4);	

		Max = std::max( Max, pop1[0].f);

	}
	pupPop = pop1;

	return 1.0;
}

bool GeneticAlgo::myDataSortPredicate(const PopulationRow& d1, const PopulationRow& d2) {
	return d1.f > d2.f;
}


vector <PopulationRow> GeneticAlgo::setAllF(vector <PopulationRow> pop) {
	for (int i = 0 ; i < pop.size() ; i++) {
		pop[i].f = sinc(pop[i].x, pop[i].y);
	}
	return pop;
}

vector <PopulationRow> GeneticAlgo::initialize(vector <PopulationRow> pop) {
	setprecision(3);

	for (int i = 0 ; i < populationCount ; i++) {
		PopulationRow pr;

		pr.x = 0;
		pr.y = 0;

		while(pr.x == 0)
			pr.x = rangedRand(-10,10);
		pr.x += (double)rangedRand(1,999)/1000;

		while (pr.y == 0)
			pr.y = rangedRand(-10,10);
		pr.y += (double)rangedRand(1,999)/1000;

		pr.f = sinc(pr.x, pr.y);

		pop.push_back(pr);

		Max = -infinity;
	}
	return pop;
}

double GeneticAlgo::sinc (double x, double y){
	if( x != 0 && y != 0 )
		return (sin(x) / x) * (sin(y) / y);
	else if ( x == 0 && y != 0 ){
		x = .001;
		return (sin(x) / x) * (sin(y) / y);
	}
	else if ( x != 0 && y == 0 ){
		y = .001;
		return (sin(x) / x) * (sin(y) / y);
	}
	else if ( x == 0 && y == 0 ){
		x = .001;
		y = .001;
		return (sin(x) / x) * (sin(y) / y);
	}
}

//__inline double GeneticAlgo::fun2(double x, double y){
//	return( 21.5 + (x * sin(4*PI*x)) + (y * sin(20*PI*y)) );
//}

__inline int GeneticAlgo::rangedRand(int range_min, int range_max){
	return (double)rand() / (RAND_MAX + 1.0) * (range_max - range_min) + range_min;
}

vector <PopulationRow> GeneticAlgo::selection1( vector <PopulationRow> pop){
	double sumOfFunValue = 0;

	for( int i = 0 ; i < pop.size() ; i++ ){
		pop[i].fitnes = 500 * (pop[i].f + 1);
	}

	for( int i = 0 ; i < pop.size() ; i++ ){
		sumOfFunValue += pop[i].fitnes;
	}

	// Sort the vector using predicate and std::sort
	std::sort(pop.begin(), pop.end(), myDataSortPredicate);

	int d1 = 0, d2 ,j;
	for( j = 0 ; j < pop.size() ; j++ ){
		pop[j].percetConfine[0] = d1;
		pop[j].percetConfine[1] = (pop[j].fitnes / sumOfFunValue * 1000) + pop[j].percetConfine[0];
		d1 = pop[j].percetConfine[1] + 1;
	}

	int max = pop[j-1].percetConfine[1];

	vector <PopulationRow> tempPop;

	for( int n = 0 ; n < pop.size() ; n++ ){
		int randNum = rand() % max;
		for( int i = 0 ; i < pop.size() ; i++ ){
			if ( randNum >= pop[i].percetConfine[0] && randNum <= pop[i].percetConfine[1]){
				tempPop.push_back(pop[i]);
				break;
			}
		}
	}

	return tempPop;
}

vector <PopulationRow> GeneticAlgo::selection2( vector <PopulationRow> pop){
	pop = setAllF(pop);
	std::sort(pop.begin(), pop.end(), myDataSortPredicate);

	while (pop.size() > populationCount){
		pop.pop_back();
	}
	return pop;
}

vector <PopulationRow> GeneticAlgo::crossOverpopulation( vector <PopulationRow> coPop ){
	vector <PopulationRow> resultPop;
	int count = 0;
	if (coPop.size() % 2 == 0)
		count = coPop.size();
	else
		count = coPop.size() - 1;

	if ( coPop.size() > 0){
		for (int i = 0 ; i < count ; i+=2 ){
			int randNum = rand() % 11;
			if (randNum < 7){
				vector <PopulationRow> crossPop;
				crossPop.push_back(coPop[i]);
				crossPop.push_back(coPop[i+1]);
				crossPop = crossOver(crossPop);
				resultPop.push_back(crossPop[0]);
				resultPop.push_back(crossPop[1]);
			}
		}
	}
	return resultPop;
}

vector <PopulationRow> GeneticAlgo::mutationpopulation( vector <PopulationRow> pop){
	int randNum = 0;
	vector <PopulationRow> mutPop;
	for (std::vector<PopulationRow>::iterator it = mutPop.begin(); it != mutPop.end(); ++it){
		it->x = mutation(it->x);
		it->y = mutation(it->y);
	}

	return pop;
}

double GeneticAlgo::mutation(double f)
{
	int randNum = 0;

	Bits bdouble = convertdoubleToBitStruct(f);

	for (int i = 5 ; i >= 2 ; i--, i /= 2 ){
		randNum = rand() % populationCount;

		if (randNum < 30){
			switch( i ){
			case 2:
				bdouble.ib2 = ~ bdouble.ib2;
				break;
			case 3:
				bdouble.ib3 = ~ bdouble.ib3;
				break;
			case 4:
				bdouble.ib4 = ~ bdouble.ib4;
				break;
			case 5:
				bdouble.ib5 = ~ bdouble.ib5;
				break;
			}
		}
	}

	for (int x = 10 ; x >= 1 ; x--, x /= 2 ){
		randNum = rand() % populationCount;
		if (randNum < 5){
			switch( x ){
			case 1:
				bdouble.db1 = ~ bdouble.db1;
				break;
			case 2:
				bdouble.db2 = ~ bdouble.db2;
				break;
			case 3:
				bdouble.db3 = ~ bdouble.db3;
				break;
			case 4:
				bdouble.db4 = ~ bdouble.db4;
				break;
			case 5:
				bdouble.db5 = ~ bdouble.db5;
				break;
			case 6:
				bdouble.db6 = ~ bdouble.db6;
				break;
			case 7:
				bdouble.db7 = ~ bdouble.db7;
				break;
			case 8:
				bdouble.db8 = ~ bdouble.db8;
				break;
			case 9:
				bdouble.db9 = ~ bdouble.db9;
				break;
			case 10:
				bdouble.db10 = ~ bdouble.db10;
				break;
			}
		}
	}
	return convertBitStructTodouble(bdouble);
}


vector <PopulationRow> GeneticAlgo::crossOver(vector <PopulationRow> crossPop){
	PopulationRow pr1;
	PopulationRow pr2;

	pr1 = crossPop[0];
	pr2 = crossPop[1];

	Bits x1, x2, y1, y2;
	Bits tempBit;
	x1 = convertdoubleToBitStruct(pr1.x);
	x2 = convertdoubleToBitStruct(pr2.x);
	y1 = convertdoubleToBitStruct(pr1.y);
	y2 = convertdoubleToBitStruct(pr2.y);

	int randNum = rand() % 3;

	switch (randNum){
	case 0:
		{
			tempBit = x1;

			x1.ib2 = x2.ib2;
			x1.ib4 = x2.ib4;
			x2.ib2 = tempBit.ib2;
			x2.ib4 = tempBit.ib4;

			x1.db2 = x2.db2;
			x1.db4 = x2.db4;
			x1.db6 = x2.db6;
			x1.db8 = x2.db8;
			x1.db10 = x2.db10;
			x2.db2 = tempBit.db2;
			x2.db4 = tempBit.db4;
			x2.db6 = tempBit.db6;
			x2.db8 = tempBit.db8;
			x2.db10 = tempBit.db10;

			tempBit = y1;

			y1.ib2 = y2.ib2;
			y1.ib4 = y2.ib4;
			y2.ib2 = tempBit.ib2;
			y2.ib4 = tempBit.ib4;

			y1.db2 = y2.db6;
			y1.db4 = y2.db7;
			y1.db6 = y2.db8;
			y1.db8 = y2.db9;
			y1.db10 = y2.db10;
			y2.db2 = tempBit.db2;
			y2.db4 = tempBit.db4;
			y2.db6 = tempBit.db6;
			y2.db8 = tempBit.db8;
			y2.db10 = tempBit.db10;

			break;
		}
	case 1:
		{
			tempBit = x1;

			x1.ib3 = x2.ib3;
			x1.ib5 = x2.ib5;
			x2.ib3 = tempBit.ib3;
			x2.ib5 = tempBit.ib5;

			x1.db1 = x2.db6;
			x1.db3 = x2.db7;
			x1.db5 = x2.db8;
			x1.db7 = x2.db9;
			x1.db9 = x2.db10;
			x2.db1 = tempBit.db1;
			x2.db3 = tempBit.db3;
			x2.db5 = tempBit.db5;
			x2.db7 = tempBit.db7;
			x2.db9 = tempBit.db9;

			tempBit = y1;

			y1.ib3 = y2.ib3;
			y1.ib5 = y2.ib5;
			y2.ib3 = tempBit.ib3;
			y2.ib5 = tempBit.ib5;

			y1.db1 = y2.db6;
			y1.db3 = y2.db7;
			y1.db5 = y2.db8;
			y1.db7 = y2.db9;
			y1.db9 = y2.db10;
			y2.db1 = tempBit.db1;
			y2.db3 = tempBit.db3;
			y2.db5 = tempBit.db5;
			y2.db7 = tempBit.db7;
			y2.db9 = tempBit.db9;
			break;
		}
	case 2:
		{
			tempBit = x1;

			x1 = y2;
			y2 = tempBit;

			tempBit = x2;

			x2 = y1;
			y1 = tempBit;

			break;
		}
	}

	pr1.x = convertBitStructTodouble(x1);
	pr2.x = convertBitStructTodouble(x2);
	pr1.y = convertBitStructTodouble(y1);
	pr2.y = convertBitStructTodouble(y2);

	pr1.f = sinc(pr1.x, pr1.y);
	pr2.f = sinc(pr2.x, pr2.y);

	crossPop[0] = pr1;
	crossPop[1] = pr2;

	return crossPop;

}

Bits GeneticAlgo::convertdoubleToBitStruct( double x ){
	double ti;
	double tf;
	tf = modf( x, &ti );
	int f = (int)( tf * 1000);
	int i = (int)ti;

	Bits tempBit;

	if (i < 0)
		tempBit.ib1 = 1;
	else 
		tempBit.ib1 = 0;

	i = abs (i);
	for (int j = 5 ; j >= 2 ; j--, i /= 2 )
	{
		switch( j )
		{
		case 2:
			tempBit.ib2 = i % 2;
			break;
		case 3:
			tempBit.ib3 = i % 2;
			break;
		case 4:
			tempBit.ib4 = i % 2;
			break;
		case 5:
			tempBit.ib5 = i % 2;
			break;
		}
	}

	for (int j = 10 ; j >= 1 ; j--, f /= 2 )
	{
		switch( j )
		{
		case 1:
			tempBit.db1 = f % 2;
			break;
		case 2:
			tempBit.db2 = f % 2;
			break;
		case 3:
			tempBit.db3 = f % 2;
			break;
		case 4:
			tempBit.db4 = f % 2;
			break;
		case 5:
			tempBit.db5 = f % 2;
			break;
		case 6:
			tempBit.db6 = f % 2;
			break;
		case 7:
			tempBit.db7 = f % 2;
			break;
		case 8:
			tempBit.db8 = f % 2;
			break;
		case 9:
			tempBit.db9 = f % 2;
			break;
		case 10:
			tempBit.db10 = f % 2;
			break;
		}
	}
	return tempBit;
}

double GeneticAlgo::convertBitStructTodouble (Bits b){
	int p = 0;
	double f;
	double f2;

	f = b.ib5;
	f += b.ib4 * 2;
	f += b.ib3 * 4;
	f += b.ib2 * 8;
	f = b.ib1 ? -f : f ;

	f2 = b.db10;
	f2 += b.db9 * 2;
	f2 += b.db8 * 4;
	f2 += b.db7 * 8;
	f2 += b.db6 * 16;
	f2 += b.db5 * 32;
	f2 += b.db4 * 64;
	f2 += b.db3 * 128;
	f2 += b.db2 * 256;
	f2 += b.db1 * 512;

	f += f2 / 1000;

	return f;
}

double GeneticAlgo::getMaximum(){
	return Max;
}
