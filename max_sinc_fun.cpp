// Max Of Sinc Function - Using Genetic Algorithm - By Mohsen Rashidi
// codeblog.ir
// mohsenrsd@gmail.com

#include "genetic_algorithm.h"
#include <iostream>
#include <vector>
using namespace std;

void runSinc(int iterateNumber, int popCount) {
	GeneticAlgo sinc(iterateNumber, popCount);
	sinc.findMaxOfFunction();
	cout << std::fixed;
	cout << std::setprecision(4);
	cout << "\n -----X-----" << " -----Y----- " << "-----F-----" << endl;
	for (std::vector<PopulationRow>::iterator it = sinc.pupPop.begin(); it != sinc.pupPop.end(); ++it){
		cout << setw(10) << it->x << setw(12) << it->y << setw(12) << it->f << endl;
	}
	cout << "\nResult: " << sinc.getMaximum() << "\n\n";
}

int main() {
	while (1){
		int iteration = 0, population = 0;
		cout << "\n****************************\n" << endl;
		cout << "Iteration: ";
		cin >> iteration;
		cout << "\nPopulation Count: ";
		cin >> population;
		runSinc(iteration, population);
	}
	return 0;
}
